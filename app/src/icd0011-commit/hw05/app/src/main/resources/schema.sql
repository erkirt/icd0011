
DROP  TABLE IF EXISTS orders;

DROP  TABLE IF EXISTS orderRows;

DROP SEQUENCE IF EXISTS seq1;

CREATE SEQUENCE seq1 START WITH 1;

CREATE TABLE orders (
    id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('seq1'),
    orderNumber VARCHAR(255) NOT NULL
);

CREATE TABLE orderRows (
    id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('seq1'),
    order_id int,
    itemName VARCHAR(255),
    quantity int,
    price int
);

