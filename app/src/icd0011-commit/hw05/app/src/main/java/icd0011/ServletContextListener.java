package icd0011;

import util.ConfigUtil;
import util.ConnectionInfo;
import util.ConnectionPoolFactory;
import util.FileUtil;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServlet;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

@WebListener
public class ServletContextListener implements javax.servlet.ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce)  {
        ConnectionPoolFactory pool = new ConnectionPoolFactory();
        DataSource source = pool.createConnectionPool();

        ServletContext context = sce.getServletContext();
        context.setAttribute("Source", source);

        try (Connection source1 = source.getConnection(); Statement stmt = source1.createStatement()) {

            String sql1 = FileUtil.readFileFromClasspath("schema.sql");

            stmt.executeUpdate(sql1);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }



    }



}
