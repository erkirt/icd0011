package people_and_stuff;

import lombok.Data;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;



@Data
public class OrderDao {

    private final DataSource dataSource;

    public OrderDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Order> findOrder() {

        try (Connection conn = dataSource.getConnection(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(
                    "select id, orderNumber from orders");

            ArrayList<Order> orders = new ArrayList<>();

            while (rs.next()){
                Order order = new Order(rs.getLong("id"), rs.getString("orderNumber"));

                orders.add(order);
            }
            return orders;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public Order findOrderById(long id) {

        String sql = "select id, orderNumber from orders where id = ?";

        try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);

            ResultSet rs = ps.executeQuery();

            if (rs.next()){
                return new Order(rs.getLong("id"), rs.getString("orderNumber"));

            }
            return null;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Order insertOrder(Order order){
        String sql = "insert into orders(orderNumber) values(?)";
        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql, new String[]{"id"})) {

            ps.setString(1, order.getOrderNumber());

            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();

            if (!rs.next()){
                throw new RuntimeException("unexpected");
            }

            return new Order(rs.getLong("id"), order.getOrderNumber());

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public OrderRows insertOrderRow(OrderRows orderRows){
        String sql = "insert into orderrows(order_id, itemname, quantity, price) values(?,?,?,?)";
        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql, new String[]{"id"})) {

            ps.setInt(1, orderRows.getOrderId());
            ps.setString(2, orderRows.getItemName());
            ps.setInt(3, orderRows.getQuantity());
            ps.setInt(4, orderRows.getPrice());

            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();

            if (!rs.next()){
                throw new RuntimeException("unexpected");
            }

            return new OrderRows(rs.getLong("id"), orderRows.getOrderId(),orderRows.getItemName(), orderRows.getQuantity(), orderRows.getPrice());

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }






}
