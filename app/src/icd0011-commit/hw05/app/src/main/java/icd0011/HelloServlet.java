package icd0011;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import javax.xml.crypto.Data;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import people_and_stuff.Order;
import people_and_stuff.OrderDao;
import util.ConfigUtil;
import util.ConnectionPoolFactory;
import util.DataSourceProvider;
import util.Util;

@WebServlet("/api/orders")
public class HelloServlet extends HttpServlet {


    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException {


        DataSource source = (DataSource) getServletContext().getAttribute("Source");
        OrderDao order1 = new OrderDao(source);
        String orderId = request.getParameter("id");
        String json;
        if (orderId == null) {
            json = new Gson().toJson(order1.findOrder());
        } else {
            json = new Gson().toJson(order1.findOrderById(Long.parseLong(request.getParameter("id"))));
        }
        response.setContentType("application/json");
        response.getWriter().print(json);
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {


        Object source = getServletContext().getAttribute("Source");
        if (source instanceof DataSource) {
            OrderDao order1 = new OrderDao((DataSource) source);
            String ordering = Util.readStream(request.getInputStream());
            Order order = new ObjectMapper().readValue(ordering, Order.class);
            String json = new Gson().toJson(order1.insertOrder(order));
            resp.setContentType("application/json");
            resp.getWriter().print(json);
        }
    }
}