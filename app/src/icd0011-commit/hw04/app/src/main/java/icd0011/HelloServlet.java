package icd0011;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import people_and_stuff.Order;
import util.Util;

@WebServlet("/api/orders")
public class HelloServlet extends HttpServlet {

    private long id = 0L;

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException {


        ServletContext context = getServletContext();
        Order order = (Order) context.getAttribute("orders");
        String json = new ObjectMapper().writeValueAsString(order);
        response.setContentType("application/json");
        response.getWriter().print(json);
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {

        String json = Util.readStream(request.getInputStream());
        Order order = new ObjectMapper().readValue(json , Order.class);

        if (order.getId() == null){
            id++;
            order.setId(id);
        }
        getServletContext().setAttribute("orders", order);
        String response = new ObjectMapper().writeValueAsString(order);
        resp.setContentType("application/json");

        resp.getWriter().println(response);


    }
}