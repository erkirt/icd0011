package icd0011;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import javax.xml.crypto.Data;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import people_and_stuff.Order;
import people_and_stuff.OrderDao;
import util.ConfigUtil;
import util.ConnectionPoolFactory;
import util.DataSourceProvider;
import util.Util;

@WebServlet("/api/orders")
public class HelloServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException {


        DataSource source = (DataSource) getServletContext().getAttribute("Source");
        OrderDao order1 = new OrderDao(source);
        String orderId = request.getParameter("id");
        String json;
//        System.out.println(orderId);
        if (orderId == null) {
            json = new Gson().toJson(order1.findOrder());
        } else {
//            System.out.println(order1.findOrderById(Long.parseLong(orderId)));
            json = new Gson().toJson(order1.findOrderById(Long.parseLong(request.getParameter("id"))));
//            json = new Gson().toJson(order1.findOrderById(2));
        }
//        System.out.println(json);
        response.setContentType("application/json");
        response.getWriter().print(json);
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {


        Object source = getServletContext().getAttribute("Source");
        if (source instanceof DataSource) {
            OrderDao orderdao = new OrderDao((DataSource) source);
            String ordering = Util.readStream(request.getInputStream());
            Order order = new ObjectMapper().readValue(ordering, Order.class);

            Order orderWithId = orderdao.insertOrder(order);

            if (order.getOrderRows() != null){

                for (int i = 0; i < order.getOrderRows().size(); i++) {

                    String itemName = order.getOrderRows().get(i).getItemName();
                    Integer quantity = order.getOrderRows().get(i).getQuantity();
                    Integer price = order.getOrderRows().get(i).getPrice();

                    orderdao.insertOrderRow(orderWithId.getId(),itemName,quantity,price);
                }

                orderWithId.setOrderRows(orderdao.getOrderRowsById(orderWithId.getId()));
//                System.out.println(orderWithId.getId());
//                System.out.println(orderdao.getOrderRowsById(orderWithId.getId()));
            }

            String json = new Gson().toJson(orderWithId);
//            System.out.println("see on doPost");
//            System.out.println(json);
            resp.setContentType("application/json");
            resp.getWriter().print(json);
        }
    }
    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        String id = req.getParameter("id");
        Long idValue = Long.parseLong(id);

        Object source = getServletContext().getAttribute("Source");
        OrderDao order = new OrderDao((DataSource) source);

        order.deleteRowById(idValue);


    }

}
