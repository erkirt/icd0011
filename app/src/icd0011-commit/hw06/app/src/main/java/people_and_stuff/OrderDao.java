package people_and_stuff;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;



@Data
public class OrderDao {

    private final DataSource dataSource;

    public OrderDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Order> findOrder() {

        try (Connection conn = dataSource.getConnection(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(
                    "select id, orderNumber from orders");

            ArrayList<Order> orders = new ArrayList<>();

            while (rs.next()){
                Order order = new Order(rs.getLong("id"), rs.getString("orderNumber"), getOrderRowsById(rs.getLong("id")));


                orders.add(order);
            }
            return orders;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public Order findOrderById(long id) {

        String sql = "select * from orders left join orderrows on orders.id= orderrows.order_id where orders.id = ?";

        try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);
            List<OrderRows> orderRows = new ArrayList<>();

            ResultSet rs = ps.executeQuery();
            Order order123 = null;

            while (rs.next()){
                OrderRows row = new OrderRows(rs.getString("itemName"), rs.getInt("quantity"), rs.getInt("price"));

                orderRows.add(row);

                order123 = new Order(rs.getLong("id"), rs.getString("orderNumber"),null);
                order123.setOrderRows(orderRows);

            }
            return order123;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Order insertOrder(Order order){
        String sql = "insert into orders(orderNumber) values(?)";
        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql, new String[]{"id"})) {

            ps.setString(1, order.getOrderNumber());

            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();

            if (!rs.next()){
                throw new RuntimeException("unexpected");
            }

            return new Order(rs.getLong("id"), order.getOrderNumber(), null);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public Long insertOrderRow(Long orderId, String itemName, Integer quantity ,Integer price){
        String sql = "insert into orderrows(order_id, itemName, quantity, price) values (?, ? ,?, ?)";

        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql, new String[]{"id"})) {


            ps.setLong(1, orderId);
            ps.setString(2, itemName);
            ps.setInt(3, quantity);
            ps.setInt(4, price);

            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();

            if (!rs.next()){
                throw new RuntimeException("unexpected");
            }

            return rs.getLong("id");

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<OrderRows> getOrderRowsById(long idOfOrder) {

        String sql = "select * from orderrows where orderrows.order_id = ?";


        try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, idOfOrder);
            List<OrderRows> orderRows = new ArrayList<>();

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                OrderRows row = new OrderRows(rs.getString("itemName"), rs.getInt("quantity"), rs.getInt("price"));

                orderRows.add(row);

            }
            return orderRows;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


        public void deleteRowById(Long id) {

        String sql = "delete from orders where id =?";

        try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);

        }

    }





}
