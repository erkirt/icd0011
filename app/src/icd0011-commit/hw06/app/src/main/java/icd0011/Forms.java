package icd0011;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import people_and_stuff.Order;
import util.Util;

@WebServlet("/orders/form")
public class Forms extends HttpServlet {

    private long id = 0L;

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException {

        ServletContext context = getServletContext();
        Order order = (Order) context.getAttribute("orders");
        response.setContentType("application/x-www-form-urlencoded");
        response.getWriter().print(order);
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {

        Order order = new Order();
        String id = request.getParameter("id");

        if (id == null){
            id = String.valueOf(this.id);
            this.id++;
        }
        order.setId(Long.valueOf(id));
        order.setOrderNumber(request.getParameter("orderNumber"));
        getServletContext().setAttribute("orders", order);

        resp.setContentType("application/x-www-form-urlencoded");
        resp.getWriter().println(order.getId());
    }
}