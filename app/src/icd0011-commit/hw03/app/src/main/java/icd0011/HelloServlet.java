package icd0011;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import people_and_stuff.Order;
import util.Util;

@WebServlet("/api/orders")
public class HelloServlet extends HttpServlet {

    private long id = 1;

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException {

        response.setContentType("application/json");

        response.getWriter().print("{}");
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {

        String json = Util.readStream(request.getInputStream());
        json = json.replace("{", "").replace("}", "").trim();
        String[] pairs = json.split(",");

        Map<String, String> map = new LinkedHashMap<>();

        for (String pair : pairs) {
            String[] keyAndValue = pair.split(":");
            String key = keyAndValue[0].replaceAll("\"", "").trim();
            String value = keyAndValue[1].replaceAll("\"", "").trim();
            map.put(key,value);
        }

        Order order = new Order(id, map.get("orderNumber"));
        id++;
        String response = "{ \"id\": \""+ order.getId() +"\", "+ "\"orderNumber\": \""+order.getOrderNumber()+"\" }";
        resp.setContentType("application/json");

        resp.getWriter().println(response);
    }
}