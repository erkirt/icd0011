package conf;

import org.springframework.context.annotation.*;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import javax.sql.DataSource;


@Configuration
@PropertySource("classpath:/application.properties")
@ComponentScan(basePackages = {"dao", "icd0011", "people_and_stuff", "validation"})
@EnableAspectJAutoProxy
public class Config {


    @Bean
    public JdbcTemplate getTemplate(DataSource dataSource) {

        var populator = new ResourceDatabasePopulator(
                new ClassPathResource("schema.sql")
//                , new ClassPathResource("data.sql")
        );

        DatabasePopulatorUtils.execute(populator, dataSource);

        return new JdbcTemplate(dataSource);
    }


}