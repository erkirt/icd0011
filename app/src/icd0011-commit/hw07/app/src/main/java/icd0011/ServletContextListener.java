package icd0011;

import conf.Config;
import conf.HsqlDataSource;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.annotation.WebListener;


@WebListener
public class ServletContextListener implements javax.servlet.ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(Config.class, HsqlDataSource.class);

        ServletContext context = sce.getServletContext();
        context.setAttribute("ctx", ctx);


    }


}
