package icd0011;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import dao.OrderDao;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import people_and_stuff.Order;
import util.Util;
import validation.ValidationErrors;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import java.io.IOException;

@WebServlet("/api/orders")
public class HelloServlet extends HttpServlet {


    private AnnotationConfigApplicationContext annotationConfigApplicationContext;


    @Override
    public void init() {
        ServletContext context = getServletContext();

        this.annotationConfigApplicationContext = (AnnotationConfigApplicationContext) context.getAttribute("ctx");
    }

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException {


        String orderId = request.getParameter("id");

        OrderDao orderdao = annotationConfigApplicationContext.getBean(OrderDao.class);

        String json;

        if (orderId == null) {
            json = new Gson().toJson(orderdao.findOrder());
//                System.out.println("Ilma id-ta");
//                System.out.println(json);
        } else {
            json = new Gson().toJson(orderdao.findOrderById(Long.parseLong(orderId)));
//                System.out.println("Koos id-ga");
//                System.out.println(json);
        }
        response.setContentType("application/json");
        response.getWriter().print(json);

    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {


        OrderDao orderdao = annotationConfigApplicationContext.getBean(OrderDao.class);


        String ordering = Util.readStream(request.getInputStream());
        Order order = new ObjectMapper().readValue(ordering, Order.class);


        var validator = Validation.buildDefaultValidatorFactory().getValidator();
        var violations = validator.validate(order);
        final ValidationErrors errors = new ValidationErrors();
        for (ConstraintViolation<Order> violation : violations) {
            errors.addErrorMessage(violation.getMessage());
        }
        String json = new ObjectMapper().writeValueAsString(errors);
        if (errors.hasErrors()){
            resp.setHeader("Content-Type", "application/json");
            resp.getWriter().print(json);
            resp.setStatus(400);

        }

        else {
            Order finalOrder = orderdao.insertOrder(order);
            resp.setContentType("application/json");
            resp.getWriter().print(new Gson().toJson(finalOrder));
        }
    }


    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        String id = req.getParameter("id");
        Long idValue = Long.parseLong(id);

        OrderDao order = annotationConfigApplicationContext.getBean(OrderDao.class);
        order.deleteRowById(idValue);
    }
}
