package dao;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import people_and_stuff.Order;
import people_and_stuff.OrderRows;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Repository
@Data
public class OrderDao {


    private JdbcTemplate template;

    @Autowired
    public OrderDao(JdbcTemplate template) {

        this.template = template;
    }

    public List<Order> findOrder() {

        String sql = "select id, orderNumber from orders";

        List<Order> orders = template.query(sql, new BeanPropertyRowMapper<>(Order.class));

        for (Order order : orders) {
            order.setOrderRows(findOrderRowById(order.getId()));
        }
        return orders;
    }


    public Order findOrderById(long id) {
        String sql = "select * from orders left join \"orderrows\" on orders.id= \"orderrows\".order_id where orders.id = ?";
        List<Map<String, Object>> list = template.queryForList(sql, id);
        List<OrderRows> orderrows = new ArrayList<>();

        Order order = null;


        for (Map<String, Object> map : list) {


            OrderRows row = new OrderRows((String) map.get("itemName"), (Integer) map.get("quantity"), (Integer) map.get("price"));

            orderrows.add(row);

            Integer idV = (Integer) map.get("id");
            Long idL = Long.valueOf(idV);


            order = new Order(idL, (String) map.get("orderNumber"), orderrows);


        }


        return order;
    }


    public List<OrderRows> findOrderRowById(long id) {
        String sql = "select * from \"orderrows\" where \"orderrows\".order_id = ?";

        List<Map<String, Object>> list = template.queryForList(sql, id);

        List<OrderRows> orderrows = new ArrayList<>();


        for (Map<String, Object> map : list) {

            OrderRows row = new OrderRows((String) map.get("itemName"), (Integer) map.get("quantity"), (Integer) map.get("price"));

            orderrows.add(row);

        }

        return orderrows;
    }


    public Order insertOrder(Order order) {
        var data = new BeanPropertySqlParameterSource(order);

        Number id = new SimpleJdbcInsert(template)
                .withTableName("orders")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(data);

        if (order.getOrderRows() != null) {

            for (int i = 0; i < order.getOrderRows().size(); i++) {
                String itemName = order.getOrderRows().get(i).getItemName();
                Integer quantity = order.getOrderRows().get(i).getQuantity();
                Integer price = order.getOrderRows().get(i).getPrice();
                String sql = "insert into \"orderrows\"(order_id, itemName, quantity, price) values (?, ? ,?, ?)";
                template.update(sql, id.longValue(), itemName, quantity, price);

            }

        }
        order.setId(id.longValue());
        return new Order(order.getId(), order.getOrderNumber(), order.getOrderRows());
    }

    public void deleteRowById(Long id) {

        String sql = "delete from orders where id =?";

        template.update(sql, id);

    }


}
