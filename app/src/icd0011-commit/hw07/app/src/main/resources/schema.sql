CREATE SEQUENCE IF NOT EXISTS seq1 START WITH 1;
CREATE SEQUENCE IF NOT EXISTS seq2 START WITH 1;


DROP  TABLE IF EXISTS orderrows;
DROP  TABLE IF EXISTS orders;

CREATE TABLE IF NOT EXISTS orders (
                                      id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('seq1'),
                                      orderNumber VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS "orderrows" (
                                           id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('seq2'),
                                           order_id int,
                                           itemName VARCHAR (255) NOT NULL,
                                           quantity int,
                                           price int
--                              CONSTRAINT orders_fkey FOREIGN KEY (order_id) REFERENCES orders(id)
);

