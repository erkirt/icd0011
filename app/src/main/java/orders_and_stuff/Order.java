package orders_and_stuff;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Order {
    private Long id;


    @NotNull
    @Size(min = 2, max = 50)
    private String orderNumber;

    @Valid
    private List<OrderRows> orderRows;


}
