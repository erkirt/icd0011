package controller;

import dao.OrderDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import orders_and_stuff.Order;

import javax.validation.Valid;
import java.util.List;


@RestController
public class OrderController {


    @Autowired
    private final OrderDao dao;


    public OrderController(OrderDao dao) {
        this.dao = dao;
    }

    @GetMapping("orders")
    public List<Order> getOrder() {
        return dao.findOrder();
    }


    @GetMapping("orders/{id}")
    public Order getOrderById(@PathVariable Long id) {

        return dao.findOrderById(id);
    }

    @PostMapping("orders")
    public Order saveOrder(@RequestBody @Valid Order order) {

        return dao.insertOrder(order);
    }

    @DeleteMapping("orders/{id}")
    public void deleteOrder(@PathVariable Long id) {

        dao.deleteRowById(id);
    }

}